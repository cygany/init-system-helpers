This is the init-system-helpers package from the salsa.debian.org, with our minor modifications
to become compilable under cygany, which a debian overlay over cygwin.

Branch tracks master in the salsa repository, roughly so:

Git remotes:
```
origin   git@gitlab.com:cygany/init-system-helpers.git (fetch)
origin   git@gitlab.com:cygany/init-system-helpers.git (push)
upstream https://salsa.debian.org/debian/init-system-helpers.git (fetch)
upstream https://salsa.debian.org/debian/init-system-helpers.git (push)
```

Branch setup:
```
* cygany  7ced903 [origin/cygany] ...
  master  ba826f2 [upstream/master] ...
```

In our current state, it can be only compiled roughly so:

```
PERL5LIB=/usr/share/perl5 DEB_RULES_REQUIRES_ROOT=no debian/rules build binary
```

But soon it will be hopefully better.
